import { connect } from 'react-redux'
import MoonshotList from '../components/Moonshot'

export const MoonshotListContainer = connect(state => ({
  moonshots: state.moonshots
}), {})(MoonshotList)