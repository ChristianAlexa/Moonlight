import React from 'react'
import './App.css'
import Header from './components/Header'
import MoonshotForm from './components/MoonshotForm'

const App = () => {
  return (
    <div>
      <Header />
      <MoonshotForm />
    </div>
  )
}

export default App;
