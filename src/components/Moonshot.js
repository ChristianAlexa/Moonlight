import React from 'react'

const Moonshot = ({title}) => {

  const titleStyle = {
    color: 'red',
    padding: '2px',
    marginLeft: '5px'
  }
  const card = {
    height: '30px',
    width: '150px',
    border: '1px solid black',
    borderRadius: '5px',
    margin: '2px'
  }

  return (
    <div style={card}>
      <p style={titleStyle}><span role="img" aria-label="moon">🌙</span> {title}</p>
    </div>
  )
}

export default Moonshot