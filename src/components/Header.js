import React from 'react'

const Header = () => {
  const headerStyle = {
    color: 'white',
    backgroundColor: 'black',
    padding: '10px'
  }

  return (
    <div style={headerStyle}>
      <h1>Moonlight</h1>
      <p>Moonshot Experiments</p>
    </div>
  )
}

export default Header