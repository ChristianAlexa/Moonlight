import React from 'react'

const MoonshotList = ({ moonshots }) => {
  const listStyle = {
    padding: '10px'
  }
  return (
    <ul style={listStyle}>
      {moonshots.map((moonshot, index) => 
        <li key={index}>{moonshot}</li>
      )}
    </ul> 
  )
}

export default MoonshotList