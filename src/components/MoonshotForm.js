import React, {Component} from 'react'
import MoonshotList from './MoonshotList'
import Moonshot from './Moonshot'

class MoonshotForm extends Component {

  constructor(props){
    super(props)
    this.state = {
      title: '',
      moonshots: []
    }
  }

  handleChange = event => {
    this.setState({
      title: event.target.value
    }, () => {
      console.log(this.state.title)})
  }

  handleSubmit = event => {
    // prevents page from reloading
    event.preventDefault()         

    // validate that input is not empty
    if (this.state.title.trim().length !== 0){

      console.log('creating moonshot');
      let newMoonshot = <Moonshot title={this.state.title} />

      // update the state
      this.setState({
        moonshots: [...this.state.moonshots, newMoonshot],
        title: '', 
      }, () =>{
        console.log(this.state.moonshots)
      })

    } else {
      alert('enter a title')
    }
  }

  handleClear = () => {
    const proceed = window.confirm("DELETE all moonshots?")
    let emptyMoonshots = []
    proceed
      ? this.setState({
        moonshots: emptyMoonshots
      }, () => {console.log('deleted')})
      : console.log('cancelled') 
    console.clear()
  }

  render(){
    const formStyle = {
        padding: '10px'
    }
    const submitBtn = {
        color: 'white',
        cursor: 'pointer',
        padding: '5px',
        borderRadius: '5px',
        backgroundColor: 'green',
        width: '60px',
        outline: 'none',
    }
    const clearBtn = {
        margin: '10px',
        padding: '5px',
        borderRadius: '5px',
        width: '60px',
        backgroundColor: 'salmon',
        color: 'white', 
        cursor: 'pointer',
        outline: 'none'
    }
    const statusTitleStyle = {
      padding: '10px'
    }

    let statusTitle = null;
    this.state.moonshots.length !==0
      ? statusTitle = (<h4 style={statusTitleStyle}>Tracking</h4>)
      : statusTitle = (<h4 style={statusTitleStyle}>Add Moonshots!</h4>)

    return (
      <div>
        <form style={formStyle} onSubmit={this.handleSubmit}>
          <input 
            type="text" 
            value={this.state.title}
            onChange={this.handleChange}
            placeholder="title" />
          <br /><br />
        <button style={submitBtn} type="submit">Add</button>
        </form>
        <button style={clearBtn} onClick={this.handleClear}>Clear All</button>

        <br />
        {statusTitle} 
        <MoonshotList moonshots={this.state.moonshots} />
      </div>
    )
  }
}

export default MoonshotForm 
